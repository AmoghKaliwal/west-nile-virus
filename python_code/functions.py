# Base Path Declarations
base_path = "D:\GitLab\west-nile-virus"

"""  importing module  """

import sys
sys.path.insert(0, base_path + '\python_code')
import constants
import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder

""" feature engineer date attribute """

def feature_date(train, test):
    
    # convert attribute to datetime
    train['Date'] = pd.to_datetime(train['Date'])
    
    # extract day, month, year and quarter from date
    train['Day'] = train['Date'].dt.day
    train['Month'] = train['Date'].dt.month
    train['Year'] = train['Date'].dt.year
    train['Quarter'] = train['Date'].dt.quarter
    
    # convert attribute to datetime
    test['Date'] = pd.to_datetime(test['Date'])
    
    # extract day, month, year and quarter from date
    test['Day'] = test['Date'].dt.day
    test['Month'] = test['Date'].dt.month
    test['Year'] = test['Date'].dt.year
    test['Year'] = test['Date'].dt.year
    test['Quarter'] = test['Date'].dt.quarter
    
    """  Selection of attributes depending on description  """
    train.drop('Date', axis = 'columns', inplace = True)
    test.drop('Date', axis = 'columns', inplace = True)
    
    # return the dataframe
    return train, test

""" Pre-process weather data """

def weather_preprocessing(data):
    
    # replace the unwanted levels to nan
    data = data.replace(to_replace = '  T', value = np.nan)
    data = data.replace(to_replace = 'M', value = np.nan)
    
    # subset date attribute
    date_attribute = pd.DataFrame(data['Date'])
    
    # choose numeric features
    numeric_features = constants.weather_features
    numeric_features.remove('Date')
    
    # convert columns to numeric
    data_modified = data[numeric_features].apply(pd.to_numeric, axis = 1)
    
    # concatenate the numeric and date features
    data = pd.concat([date_attribute.reset_index(drop = True), 
                      data_modified], axis = 1)
        
    # to interpolate the missing values
    data = data.interpolate(method = 'piecewise_polynomial', 
                            limit_direction = 'backward', 
                            order = 5)
    
    # aggregate data at date
    weather_imp = data.groupby(['Date'], 
                               as_index = False)[constants.weather_features].mean()
    
    # convert date attribute as string
    weather_imp['Date'] = weather_imp['Date'].astype(str)
    
    # return variables for analysis
    return weather_imp

""" Pre-process hypothesis data """

def hypothesis_processing(weather, train):
    
    # choose the required features
    weather = weather[constants.weather_features]
    
    # pre-rocess the weather data for T & M levels
    weather_processed = weather_preprocessing(data = weather)
    
    # extract NumMosquitos, WnvPresent according to date
    mosquitos_date_wise = train.groupby(['Date'], 
                                        as_index = False)[['NumMosquitos']].sum()
    wnv_date_wise = train.groupby(['Date'], 
                                  as_index = False)[['WnvPresent']].sum()
    
    # unify the weather with NumMosquitos, WnvPresent for analysis
    wnv_mosquitos_dw = pd.merge(mosquitos_date_wise, wnv_date_wise, on = 'Date')
    weather_df = pd.merge(wnv_mosquitos_dw, weather_processed, on = 'Date')
    
    # return final data frame
    return weather_df

def categorial_encoding(train, test):
    
    # Assign numeric values to categorial features
    
    lbl = LabelEncoder()
    lbl.fit(list(train['Species'].values))
    train['Species'] = lbl.transform(train['Species'].values)
    lbl.fit(list(test['Species'].values))
    test['Species'] = lbl.transform(test['Species'].values)
    lbl.fit(list(train['Trap'].values))
    train['Trap'] = lbl.transform(train['Trap'].values)
    lbl.fit(list(test['Trap'].values))
    test['Trap'] = lbl.transform(test['Trap'].values)
    
    # return the dataframe
    return train, test