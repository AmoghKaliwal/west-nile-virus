# # install packages
# pip install Pillow
# conda install -c conda-forge imbalanced-learn

# Base Path Declarations
base_path = "D:\GitLab\west-nile-virus"

"""  importing module  """

import sys
sys.path.insert(0, base_path + '\python_code')
import constants
import functions
import pandas as pd

from imblearn.over_sampling import SMOTE
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials,space_eval
# from PIL import Image

"""  read input data  """

weather = pd.read_csv(base_path + r'\input_data\weather.csv')
spray = pd.read_csv(base_path + r'\input_data\spray.csv')
train = pd.read_csv(base_path + r'\input_data\train.csv')
test = pd.read_csv(base_path + r'\input_data\test.csv')

"""  Selection of attributes depending on description  """

train.drop(constants.eliminiated_features, axis = 'columns', inplace = True)
test.drop(constants.eliminiated_features, axis = 'columns', inplace = True)

"""  explore the data  """

ax = train['Species'].hist(xrot = 45, grid = False)
ax.set_xlabel("Species")
ax.set_ylabel("Species Count")
ax.set_title("Species comparision")

species_vs_virus = train[['Species', 
                          'WnvPresent']].groupby('Species', 
                                                 as_index = False).sum()
                                                 
ax = species_vs_virus.plot.bar(x = 'Species', y = 'WnvPresent', rot = 45)
ax.set_xlabel("Species")
ax.set_ylabel("West Nile Virus Present")
ax.set_title("West Nile Virus count vs Species")

location_vs_virus = train[['Latitude', 'Longitude',
                          'WnvPresent']].groupby(['Latitude', 'Longitude'], 
                                                 as_index = False).sum()
                                                 
location_vs_virus['Location'] = location_vs_virus['Latitude'].map(str) + ' ' + location_vs_virus['Longitude'].map(str)
                                                 
ax = location_vs_virus.plot.bar(x = 'Location', y = 'WnvPresent', rot = 45)
ax.set_xlabel("Location")
ax.set_ylabel("West Nile Virus count")
ax.set_title("West Nile Virus count vs Locations")

# understadning weather data
weather['Tavg'].unique()
weather['Heat'].unique()
weather['Cool'].unique()
weather['PrecipTotal'].unique()
weather['PrecipTotal'] == '  T'
weather['PrecipTotal'] == 'M'

# pre-process the data for hypothesis validation
hypothesis_data = functions.hypothesis_processing(weather, train)

ax = hypothesis_data.plot.scatter(x = 'Tavg', y = 'NumMosquitos', 
                                  s = 'WnvPresent')
ax.set_xlabel("Average Temperature in Fahrenheit")
ax.set_ylabel("Number of Mosquitos")
ax.set_title("Mosquitos vs Average temperature")

ax = hypothesis_data.plot.scatter(x = 'PrecipTotal', y = 'NumMosquitos', 
                                  s = 'WnvPresent')
ax.set_xlabel("Precipitation in INCHES")
ax.set_ylabel("Number of Mosquitos")
ax.set_title("Mosquitos vs Precipitation")

"""  Featuring engineering of date attributes  """

train, test = functions.feature_date(train, test)

"""  model building  """

train.WnvPresent.value_counts()

# ax = train['WnvPresent'].hist(xrot = 45, grid = False)
# ax.set_xlabel("WnvPresent")
# ax.set_ylabel("WnvPresent Count")
# ax.set_title("WnvPresent comparision")

# Assign numeric values to categorial features

train, test = functions.categorial_encoding(train, test)

target = train.WnvPresent.values
features = train.drop(['WnvPresent'], axis = 1)

smote = SMOTE()
x_smote, y_smote = smote.fit_resample(features, target)

# Split the data into train and test
X_train, X_test, y_train, y_test = train_test_split(x_smote, y_smote, 
                                                    test_size = 0.2, 
                                                    random_state = 42, 
                                                    shuffle = True)

def hyperopt_train_test(params):
    model = RandomForestClassifier(**params)
    model.fit(X_train,y_train)
    pred = model.predict(X_test)
    return accuracy_score(y_test, pred)

space4rf = {
    'max_depth': hp.choice('max_depth', range(1, 5)),
    'max_features': hp.choice('max_features', range(1, 5)),
    'n_estimators': hp.choice('n_estimators', range(100, 500)),
    'criterion': hp.choice('criterion', ["gini", "entropy"]),
    'n_jobs' : 4
}

best = 0
def f(params):
    global best
    acc = hyperopt_train_test(params)
    if acc > best:
        best = acc
        # print 'new best:', best, params
    return {'loss': -acc, 'status': STATUS_OK}

trials = Trials()
best = fmin(f, space4rf, algo = tpe.suggest, max_evals = 10, trials = trials)
best = space_eval(space4rf, best)
model = RandomForestClassifier(**best)
model.fit(X_train, y_train)
pred = model.predict(X_test)
accuracy_score(y_test, pred)

cm = confusion_matrix(y_test, pred)
print(classification_report(y_test, pred))
print("Accuracy score", accuracy_score(y_test, pred))
